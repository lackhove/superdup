# superdup 

... control duplicacy backups

docker / podman example:

```bash
/usr/bin/podman run --replace --rm --name superdup \
    --security-opt label=disable \
    -v /etc:/source_dirs/etc \
    -v ${PWD}/superdup:/superdup \
    registry.gitlab.com/lackhove/superdup
```

