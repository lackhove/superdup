FROM python:3

WORKDIR /superdup

RUN pip install --no-cache-dir attrs requests

RUN wget https://github.com/gilbertchen/duplicacy/releases/download/v3.2.3/duplicacy_linux_x64_3.2.3 \
&& mv duplicacy_linux_x64_3.2.3 /usr/local/bin/duplicacy && chmod +x /usr/local/bin/duplicacy

COPY superdup.py /usr/local/bin/superdup.py

CMD [ "python", "-u", "/usr/local/bin/superdup.py", "--config", "/superdup/config.ini"]
